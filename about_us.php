<?php 
require "includes/db.php";

$data=$_POST;
$errors = array();

if (isset($data['sign_out'])) {

	unset($_SESSION['logged_user']);
	unset($_SESSION['inf_user']);
}

if(isset($data['sign_in']))
{
	$login_1=$data['login_text'];
	$password_1=$data['password_text'];

	$sign=mysqli_query($connection, "SELECT * FROM `users_darenbell` WHERE `email`= '$login_1' AND `password`='$password_1' ");

	if (mysqli_num_rows($sign)!= 0)
	 {
	 	$_SESSION['logged_user']=$sign;
	 	$_SESSION['inf_user']=mysqli_fetch_row($_SESSION['logged_user']);
     }
     else
     {
     	$errors[]='Такого пользователя не существует!';
     }
}
 ?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Darenbell</title>
	<link rel="stylesheet" href="css/about_us.css">
</head>
<body>
	<header>
	    <div class="container">
			<div class="obolochka">
				<div class= "cell">
				   <a href="index.php"> <img src="img/logo.png" alt="Darenbell" class="logo"></a>
				</div> 	
				    <nav>
				        <li class="cell cell_style linkblock" onclick="location.href='index.php';">
				        	<a class="design_font">Главная</a>
				        </li>
				        <li class="cell cell_style linkblock" onclick="location.href='price.php';">
				        	<a class="design_font">Цены и услуги</a>
				        </li>
				        <li class="cell cell_style linkblock" onclick="location.href='support.php';">
				        	<a class="design_font">Поддержка</a>
				        </li>
				        <li class="cell cell_style linkblock" onclick="location.href='about_us.php';">
				        	<a class="design_font">Подробнее о нас</a>
				        </li>
				        <div class="last_cell">
				          <img class="last_img" src="img/phone_1.png" alt="phone_1">
				          <li class="last_cell_style">
				        	  <a class="last_design_font">8 (800) 555-35-35</a>
				          </li> 
				        </div>       	
				    </nav>
			</div> 
	    </div>	
	</header>

    <section>
	<div class="container">
		<div class="section_1">
			<?php 

			if (isset($_SESSION['logged_user']))
			{	
				echo '<form method="POST" action="index.php" class="Autorization">
				<div class="center_autorization">
				<p class="design_font_Autorization">'.$_SESSION['inf_user'][1].'</p>
				</div>

				<div style="float:left;">
				  <div class= "photo_login">
				  <img class="img_style_login" src="img/'.$_SESSION['inf_user'][7].'" alt="Darenbell" class="logo">
				  </div>

				  <div class= "text_profile_login">
				  <a style=" margin-left: 70px;" href=my_profile.php?name='.$_SESSION['inf_user'][1].'>Мой профиль</a>
				  </div>

				</div>

				<div class="sign_in_sign_up1">
					<div class="messages linkblock" onclick="location.href=`send_message.php`;">
						<a class="design_font_sign_up">Сообщения</a>
					</div>
					<input type="submit" value="Выход" name="sign_out" class="sign_up design_font_sign_up linkblock">
				</div>
			</form>';

			}

			else
			{
				if (isset($data['sign_in']) && mysqli_num_rows($sign)== 0)
				{
					echo '<form method="POST" action="about_us.php" class="Autorization">
				<div class= "center_autorization">
					<a class="design_font_Autorization">Авторизация</a>
				</div>
				<p class="biba">'.array_shift($errors).'</p>
				<div class="img_and_login">
					<div class="img_login">
						<img src="img/login.jpg" alt="login">
					</div>
					<div class="login">
						<input type="text" class="login_text" name="login_text" value='.@$data[`login_text`].'>
					</div>
				</div>

				<div class="img_and_password">
					<div class="img_login">
						<img src="img/key.jpg" alt="key">
					</div>
					<div class="login">
						<input type="password" class="login_text" name="password_text" value='.@$data[`password_text`].'>
					</div>
				</div>

				<div class="sign_in_sign_up">
					<div class="sign_in linkblock" onclick="location.href=`registration.php`;">
						<a class="design_font_sign_up">Регистрация</a>
					</div>
					<input type="submit" value="Вход" name="sign_in" class="sign_up design_font_sign_up linkblock">
				</div>
			</form>';
		        }
		        else
		        {
		        	
			    echo'<form method="POST" action="about_us.php" class="Autorization">
				<div class= "center_autorization">
					<a class="design_font_Autorization">Авторизация</a>
				</div>
				<div class="img_and_login">
					<div class="img_login">
						<img src="img/login.jpg" alt="login">
					</div>
					<div class="login">
						<input type="text" class="login_text" name="login_text" value='. @$data[`login_text`].'>
					</div>
				</div>

				<div class="img_and_password">
					<div class="img_login">
						<img src="img/key.jpg" alt="key">
					</div>
					<div class="login">
						<input type="password" class="login_text" name="password_text" value="'.@$data[`password_text`].'">
					</div>
				</div>

				<div class="sign_in_sign_up">
					<div class="sign_in linkblock" onclick="location.href=`registration.php`;">
						<a class="design_font_sign_up">Регистрация</a>
					</div>
					<input type="submit" value="Вход" name="sign_in" class="sign_up design_font_sign_up linkblock">
				</div>
			</form>';
		        }

			}

			 ?>
		</div>
		<div class="section_2">
			<div class="news">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2827.5848302699033!2d30.31581855614928!3d59.92543142814051!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46963101e989297f%3A0x44283cb7a30e486!2sDarenbell!5e0!3m2!1sru!2sru!4v1514291400703" width="747" height="400" frameborder="0" style="border: 1px;border-color:#7fb3d6;border-style: none none solid none;" allowfullscreen></iframe>
				<div class="header_news">
					<p class="header_news_font">Адрес:<p class="text_news">Московский пр. д.4</p></p>
					
				</div>
				<div class="header_news">
					<p class="header_news_font">Телефон:<p class="text_news">8 (800) 555-35-35</p></p>
					
				</div>
				<div class="header_news">
					<p class="header_news_font">График работы:<p class="text_news">Каждый день с 11:00-20:00</p></p>
					
				</div>
			</div>
			<div class="raiting">
				<div class="raiting_header">
					<a class="raiting_header_font">Рейтинг самых ценных клиентов</a>
				</div>
				<div>
					<div class="user">
						<a class="design_font_Autorization raiting_otstup_a">Пользователь</a>
					</div>
					<div class="points">
						<a class="design_font_Autorization raiting_otstup_b">Очки</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Галина Анатольевна
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1250</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Данил Ишугелович
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1150</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Елена Орлова
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1002</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Владимир Постол
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">800</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Евгений Одуванчиков
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">790</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Илья Кучериков
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">750</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Владимир Макаров
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">710</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Андрей Феркин
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">610</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Полина Валиулина
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">550</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Антон Дроздов
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">500</a>
					</div>
				<div class="raiting_header1">
					<a class="raiting_header_font">Рейтинг самых ценных мастеров</a>
				</div>
				<div>
					<div class="user">
						<a class="design_font_Autorization raiting_otstup_a2">Мастер</a>
					</div>
					<div class="points">
						<a class="design_font_Autorization raiting_otstup_b">Очки</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Петр Ильин
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">2200</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Роман Егоров
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">2000</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Владимир Кондрашев
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1800</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Иван Есилов
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1700</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Евгений Дронов
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1590</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Анатолий Иванович
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1300</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Андрей Гаврилов
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1200</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Анакентий Дмитров
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1100</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Анастасия Слепакова
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">1000</a>
					</div>
					<div class="user1">
						<a class="design_font_sign_up1 raiting_otstup_a1">Иван Груздев
					    </a>
					</div>
					<div class="points1">
						<a class="design_font_sign_up1 raiting_otstup_b1">900</a>
					</div>
				</div>
			</div>
		</div>
	</div>
    </section>

	<footer>
	<div class="container">
		<div class="fon_footer">
			<img class="footer_logo" src="img/logo.png" alt="logo">
			<img class="footer_phone_2" src="img/phone_2.png" alt="phone_2">
			<a class="footer_number">8 (800) 555-35-35</a>
			<a class="prava">Вся информация, размещенная на этом сайте, является собственностью «Darenbell» и охраняется Законом об авторском праве. © «Darenbell» 2016 - 2017 г.
			</a>
		</div>
	</div>	
	</footer>
</body>
</html>