<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Darenbell</title>
	<link rel="stylesheet" href="css/registration.css">
</head>
<body>
<form enctype="multipart/form-data" method="POST" class ="block_registration" action="includes/sign_up.php">
	<div class="logo">
		<img src="img/logo.png" class="img_logo">
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Логин (Имя)</p>
		</div>
		<div class="right_login">
			<input type="text" class="right_login_text" name= "login">
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Email</p>
		</div>
		<div class="right_login">
			<input type="email" class="right_login_text" name= "email">
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Дата рождения</p>
		</div>
		<div class="right_login">
			<input type="date" class="right_login_text" name= "date">
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Пол</p>
		</div>
		<div class="right_login">
			<select class="right_login_text" name="pol" id="">
				<option value="Мужской">Мужской</option>
				<option value="Женский">Женский</option>
			</select>
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Пароль</p>
		</div>
		<div class="right_login">
			<input type="password" class="right_login_text" name= "password">
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Подтверждение пароля</p>
		</div>
		<div class="right_login">
			<input type="password" class="right_login_text" name= "ver_password">
		</div>
	</div>

	<div class="login">
		<div class="left_login">
			<p class="login_text">Фото</p>
		</div>
		<div class="right_login">
			<input class="photo_text" accept="image/jpeg, image/png" type="file" name="picture"/>
		</div>
	</div>

	<div class="buttons">
		<input type="submit" class="button_left linkblock login_text_1" value="Создать" >
		<input type="button" class="button_right linkblock login_text_1" value="Отмена" onclick="location.href='index.php';">
	</div>
</form>

</body>
</html>