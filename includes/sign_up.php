<?php 

require_once "db.php";

$login_1=$_POST['login'];
$email_1=$_POST['email'];
$date_1=$_POST['date'];
$password_1=$_POST['password'];
$ver_password_1=$_POST['ver_password'];
$pol=$_POST['pol'];
date_default_timezone_set('Europe/Moscow');
$date_now = date("Y-m-d H:i:s");
$path = '../img/'; // директория для загрузки
$ext = array_pop(explode('.',$_FILES['picture']['name'])); // расширение
$new_name = $login_1.'.'.$ext; // новое имя с расширением
$full_path = $path.$new_name; // полный путь с новым именем и расширением

$errors=array();

$dubl=mysqli_query($connection, "SELECT * FROM `users_darenbell` WHERE `email`='$email_1'");
$dubl_login=mysqli_query($connection, "SELECT * FROM `users_darenbell` WHERE `login`='$login_1'");

if($login_1=='')
{
	$errors[]='Вы не ввели логин!';
}
elseif($email_1=='')
{
    $errors[]='Вы не ввели email!';
}
elseif($date_1=='')
{
    $errors[]='Вы не ввели дату!';
}
elseif($password_1=='')
{
	$errors[]='Вы не ввели пароль!';
}
elseif ($ver_password_1=='') {
	# code...
	$errors[]='Вы не ввели повтороный пароль!';
}
elseif (mysqli_num_rows($dubl_login)!= 0) {
	$errors[]='Пользователь с таким логином уже существует!';
}
elseif (mysqli_num_rows($dubl)!= 0) {
	$errors[]='Пользователь с таким email уже существует!';
}
elseif(move_uploaded_file($_FILES['picture']['tmp_name'], $full_path))
{
	if($password_1==$ver_password_1)
    {
      $insert_user= mysqli_query($connection, "INSERT INTO `users_darenbell` (`login`, `email`, `date`, `password`, `date_of_registration`, `Pol`, `Photo`) VALUES ('$login_1', '$email_1', '$date_1', '$password_1', '$date_now', '$pol', '$new_name')");

       $new_connection= mysql_connect($config['db']['server'], $config['db']['username'],$config['db']['password']);
       $sql = 'CREATE DATABASE '.$login_1.'';
      mysql_query($sql, $new_connection);
      mysql_close($new_connection);


      $new_connection= mysqli_connect($config['db']['server'], $config['db']['username'],$config['db']['password'], $login_1);

      $query ="CREATE Table dialogs
      (
       id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
       user_to VARCHAR(200) NOT NULL,
       id_dialog INT NOT NULL,
       last_date_message DATETIME NOT NULL
      )";
     $result = mysqli_query($new_connection, $query) or die("Ошибка " . mysqli_error($new_connection));

     mysqli_close($new_connection);

    }
    else
    	$errors[]="Пароли не совпадают";

}
else
   	   $errors[]="Проблема с загрузкой файла!";


if($insert_user==true && empty($errors))
{  
   header("Location: ../index.php");
}
else
	echo '<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Darenbell</title>
	<link rel="stylesheet" href="../css/registration.css">
</head>
<body>
<form enctype="multipart/form-data" method="POST" class ="block_registration" action="sign_up.php">
	<div class="logo">
		<img src="../img/logo.png" class="img_logo">
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Логин (Имя)</p>
		</div>
		<div class="right_login">
			<input type="text" class="right_login_text" name= "login">
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Email</p>
		</div>
		<div class="right_login">
			<input type="text" class="right_login_text" name= "email">
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Дата рождения</p>
		</div>
		<div class="right_login">
			<input type="date" class="right_login_text" name= "date">
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Пол</p>
		</div>
		<div class="right_login">
			<select class="right_login_text" name="pol" id="">
				<option value="Мужской">Мужской</option>
				<option value="Женский">Женский</option>
			</select>
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Пароль</p>
		</div>
		<div class="right_login">
			<input type="password" class="right_login_text" name= "password">
		</div>
	</div>
	<div class="login">
		<div class="left_login">
			<p class="login_text">Подтверждение пароля</p>
		</div>
		<div class="right_login">
			<input type="password" class="right_login_text" name= "ver_password">
		</div>
	</div>

	<div class="login">
		<div class="left_login">
			<p class="login_text">Фото</p>
		</div>
		<div class="right_login">
			<input class="photo_text" accept="image/jpeg, image/png" type="file" name="picture"/>
		</div>
	</div>

	<div class="login">
		<p class="biba">'.array_shift($errors).'</p>
	</div>

	<div class="buttons">
		<input type="submit" class="button_left linkblock login_text_1" value="Создать" >
		<input type="button" class="button_right linkblock login_text_1" value="Отмена" onclick="location.href=`../index.php`;">
	</div>
</form>

</body>
</html>';
?>

